defmodule CryptoPuppeteerX.Repo.Migrations.CreateJSONEvents do
  use Ecto.Migration

  def change do
    create table(:json_events) do
      add :source, :string, null: false
      add :event_type, :string, null: false
      add :record_json, :map, null: false
      add :created_at, :utc_datetime, null: false
    end
  end
end
