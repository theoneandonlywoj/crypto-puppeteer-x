FROM elixir:latest
LABEL author = "Wojciech Orzechowski <theoneandonlywoj@gmail.com>"

WORKDIR /crypto_puppeteer_x

RUN mix local.hex --force \
 && apt-get update \
 && mix local.rebar --force

COPY . .
RUN rm -rf _build
RUN rm -rf deps
RUN mix deps.get --only prod 
RUN mix deps.compile
RUN MIX_ENV=prod mix distillery.release
CMD sh start_foreground.sh
