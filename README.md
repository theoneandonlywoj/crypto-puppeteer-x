# CryptoPuppeteerX

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `crypto_puppeteer_x` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:crypto_puppeteer_x, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/crypto_puppeteer_x](https://hexdocs.pm/crypto_puppeteer_x).

# Distillery setup
https://hexdocs.pm/distillery/introduction/installation.html

1. Adding distillery to the mix.exs (`{:distillery, "~> 2.0"}`) file & `mix deps.get`.
2. Create initial configuration:
```bash
mix distillery.init
```
3. MIX_ENV=prod mix distillery.release

### Run in the foreground: 
```bash
_build/prod/rel/crypto_puppeteer_x/bin/crypto_puppeteer_x foreground
```
### Run in the background:
```bash
_build/prod/rel/crypto_puppeteer_x/bin/crypto_puppeteer_x start
```
### Connect to the running node:
```bash
_build/prod/rel/crypto_puppeteer_x/bin/crypto_puppeteer_x attach
```