defmodule CryptoPuppeteerX.CexIO do
  use WebSockex

  def start_link(:no_args) do
    {:ok, pid} = WebSockex.start_link("wss://ws.cex.io/ws", __MODULE__, :no_state)
    subscribe(pid)
    {:ok, pid}
  end

  def handle_connect(_conn, state) do
    {:ok, state}
  end

  def handle_frame(_frame = {:text, msg}, state) do
    msg
    |> Jason.decode!()
    |> message_handler()

    {:ok, state}
  end

  def handle_disconnect(_conn, state) do
    {:ok, state}
  end

  defp subscribtion_frame() do
    subscription_msg =
      %{
        e: "subscribe",
        rooms: ["pair-BTC-USD"]
      }
      |> Jason.encode!()

    {:text, subscription_msg}
  end

  defp subscribe(pid) do
    WebSockex.send_frame(pid, subscribtion_frame())
  end

  def message_handler(%{"e" => event_type, "data" => data}) do
    %{
      source: "CEX.IO",
      event_type: event_type,
      record_json: %{data: data},
      created_at: DateTime.utc_now |> DateTime.to_string
    }
    |> CryptoPuppeteerX.RepoQueue.add_event_to_backlog()
  end
end
