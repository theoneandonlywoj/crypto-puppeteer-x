defmodule CryptoPuppeteerX.JSONRecord do
  use Ecto.Schema
  alias Ecto.Changeset

  schema "json_events" do
    field(:source, :string, null: false)
    field(:event_type, :string, null: false)
    field(:record_json, :map, null: false)
    field(:created_at, :utc_datetime, null: false)
  end

  def changeset(event, params \\ %{}) do
    event
    |> Changeset.cast(params, [:source, :record_json, :event_type, :created_at])
    |> Changeset.validate_required([:source, :record_json, :event_type, :created_at])
  end

  def insert(event) do
    %CryptoPuppeteerX.JSONRecord{}
    |> CryptoPuppeteerX.JSONRecord.changeset(event)
    |> CryptoPuppeteerX.Repo.insert()
  end
end
