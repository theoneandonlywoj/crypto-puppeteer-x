defmodule CryptoPuppeteerX.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Starts a worker by calling: CryptoPuppeteerX.Worker.start_link(arg)
      # {CryptoPuppeteerX.Worker, arg}
      {CryptoPuppeteerX.Repo, []},
      {CryptoPuppeteerX.RepoQueue, :no_args},
      {CryptoPuppeteerX.CexIO, :no_args}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: CryptoPuppeteerX.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
