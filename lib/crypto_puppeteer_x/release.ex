defmodule CryptoPuppeteerX.Release do
  @app :crypto_puppeteer_x

  def migrate do
    load_app()

    opts = Application.get_env(:crypto_puppeteer_x, CryptoPuppeteerX.Repo)
    case CryptoPuppeteerX.Repo.__adapter__().storage_up(opts) do
      :ok -> IO.puts "Database created!"
      {:error, error} -> IO.puts error
    end

    for repo <- repos() do
      {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :up, all: true))
    end
  end

  def rollback(repo, version) do
    load_app()
    {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :down, to: version))
  end

  defp repos do
    Application.fetch_env!(@app, :ecto_repos)
  end

  defp load_app do
    Application.load(@app)
    # Application.ensure_all_started(:ssl)
  end
end