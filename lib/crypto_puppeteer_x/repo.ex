defmodule CryptoPuppeteerX.Repo do
  use Ecto.Repo,
    otp_app: :crypto_puppeteer_x,
    adapter: Ecto.Adapters.Postgres
end
