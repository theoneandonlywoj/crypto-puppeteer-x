defmodule CryptoPuppeteerX do
  @moduledoc """
  Documentation for `CryptoPuppeteerX`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> CryptoPuppeteerX.hello()
      :world

  """
  def hello do
    :world
  end
end
