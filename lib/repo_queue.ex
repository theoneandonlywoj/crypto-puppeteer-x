defmodule CryptoPuppeteerX.RepoQueue do
  use GenServer

  @name :repo_queue

  # Client
  def start_link(:no_args) do
    GenServer.start_link(__MODULE__, :no_args, name: @name)
  end

  def status() do
    pid = Process.whereis(@name)
    GenServer.call(pid, :status)
  end

  def add_event_to_backlog(event) do
    pid = Process.whereis(@name)
    GenServer.cast(pid, {:add_event_to_backlog, event})
  end

  def queue_len() do
   status() |> :queue.len()
  end

  defp schedule(msg, timeout) do
    Process.send_after(self(), msg, timeout)
  end

  # Server
  @impl true
  def init(:no_args) do
    Process.send_after(self(), :consume_backlog, 0)
    {:ok, :queue.new()}
  end

  @impl true
  def handle_call(:status, _from, state) do
    {:reply, state, state}
  end

  @impl true
  def handle_cast({:add_event_to_backlog, event}, state) do
    new_state = :queue.in(event, state)
    {:noreply, new_state}
  end

  @impl true
  def handle_info(:consume_backlog, state) do
    case :queue.out(state) do
      {{:value, event_to_preprocess}, updated_queue} ->
        case CryptoPuppeteerX.JSONRecord.insert(event_to_preprocess) do
          {:ok, _} -> :ok
          {:error, error} -> I0.inspect error
        end
        schedule(:consume_backlog, 0)
        {:noreply, updated_queue}

      {:empty, _} ->
        schedule(:consume_backlog, 100)
        {:noreply, state}
    end
  end
end
