use Mix.Config

config :crypto_puppeteer_x, ecto_repos: [CryptoPuppeteerX.Repo]

import_config "#{Mix.env()}.exs"