use Mix.Config

config :crypto_puppeteer_x, CryptoPuppeteerX.Repo,
  database: "crypto_puppeteer_x_repo",
  username: "postgres",
  password: "password",
  hostname: "localhost",
  show_sensitive_data_on_connection_error: true,
  pool_size: 10